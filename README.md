# PHP Timecop Debian/Ubuntu packages

This repo is the small amount of `debian/` files required to build the php-timecop package as a `deb` package for installing with apt/dpkg. The result is uploaded into a PPA at https://launchpad.net/~suqld/+archive/ubuntu/php-timecop

## Publishing a new package
1. Update `debian/changelog` for details of the new release. Use `dch -i` if required to bump the version number to add a new entry
2. Run `dch -r` to set the `debian/changelog` entry to be released for the current distribution
3. Commit your changes and push to GitLab
4. Download the `artifacts.zip` from the artifacts sections https://gitlab.com/suqld-public/php-timecop-debian/-/jobs/artifacts/master/browse?job=job_build (or https://gitlab.com/suqld-public/php-timecop-debian/-/jobs/artifacts/master/download?job=job_build to download the file directly)
   ```console
   wget https://gitlab.com/suqld-public/php-timecop-debian/-/jobs/artifacts/master/download?job=job_build -O artifacts.zip
   ```
5. Extract the `artifacts.zip` file
   ```console
   unzip artifacts.zip
   ```
6. Sign the changes file.
   ```console
   debsign -k KEYID php-timecop_1.2.10-1_source.changes
   ```
7. Upload to launchpad PPA.
   ```
   dput ppa:suqld/php-timecop php-timecop_1.2.10-1_source.changes
   ```